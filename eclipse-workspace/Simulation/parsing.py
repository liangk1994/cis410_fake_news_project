import csv
# import cplex

raw_input = []
with open('data/output.csv', mode ='r') as csvfile:
	csv_reader = csv.reader(csvfile, delimiter=']')
	for row in csv_reader:
		raw_input.append(row)
#print(raw_input)
node_num = 0
for row in raw_input:
	if(len(row) != 0):
		row[0] = row[0].replace("[", "")
		row[1] = row[1].replace(",[", "")
		row[2] = row[2][1:-1].split(',')
node_num = len(raw_input[0][2])
payoff = []
indiv_payoff = []
a = [0]*node_num
d = [0]*node_num
sum_a = 0;
sum_d = 0;
for i in range(len(raw_input)):
	if(len(raw_input[i])!=0):
		for x in range(len(raw_input[i][2])):
			if(raw_input[i][2][x] == 'A'):
				sum_a = sum_a + 1 
				a[x] = a[x] + 1
			elif(raw_input[i][2][x] == 'D'):
				sum_d = sum_d + 1
				d[x] = d[x] + 1
			else:
				continue
	else:
		sum_d = round((500-sum_a)/50, 2)
		sum_a = sum_a/50
		indiv_payoff.append(raw_input[i-1][0])
		indiv_payoff.append(raw_input[i-1][1])
		# indiv_payoff.append(a)
		# indiv_payoff.append(d)
		indiv_payoff.append(sum_a)
		indiv_payoff.append(sum_d)
		payoff.append(indiv_payoff)
		a = [0]*len(raw_input[0][2])
		d = [0]*len(raw_input[0][2])
		sum_a = 0
		sum_d = 0
		indiv_payoff = []
		
# print(payoff)
file_name = ""
splited_list = []
x = len(payoff[0][0])//3
resource_list = []
for i in range(len(payoff)):
	n = len(payoff[i][0])//3
	if(i == (len(payoff)-1)):
		resource_list.append(payoff[i])
		file_name = "data/payoff" + str(n+1) + ".csv"
		with open(file_name, mode='w') as writefile:
			wr = csv.writer(writefile)
			wr.writerows(resource_list)
		splited_list.append(resource_list)
		break
	if(n == x):
		resource_list.append(payoff[i])

	else:
		file_name = "data/payoff" + str(n) + ".csv"
		with open(file_name, mode='w') as writefile:
			wr = csv.writer(writefile)
			wr.writerows(resource_list)
		splited_list.append(resource_list)
		# print(resource_list)
		resource_list = []
		resource_list.append(payoff[i])
		x = n
		file_name = ""

nice_list = []
for item in splited_list:
	nice_list_line = []
	temp_line = item[0][0]
	temp_nice_line = []
	for line in item:
		if(line[0] == temp_line):
			

			temp_nice_line.append(line)
		else:
			nice_list_line.append(temp_nice_line)
			temp_line = line[0]
			temp_nice_line = []
			temp_nice_line.append(line)
	nice_list.append(nice_list_line)