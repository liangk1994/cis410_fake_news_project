import java.util.Random;

public class Edge {
	int node_1;
	int node_2;
	float P;
	Random rand = new Random();
	public Edge(int n1, int n2) {
		this.node_1 = n1;
		this.node_2 = n2;
		this.P = this.rand.nextFloat();
	}
	
	public int get_node_1() {
		return this.node_1;
	}
	
	public int get_node_2() {
		return this.node_2;
	}
	
	public float get_p() {
		return this.P;
	}
	
	
}
