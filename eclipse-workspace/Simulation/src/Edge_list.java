import java.util.ArrayList;
import java.util.Random;

public class Edge_list {
	Random rand = new Random();
	int size;
	int list_size;
	ArrayList<Edge> edge_list;
	public Edge_list(int v) {
		this.edge_list = new ArrayList<Edge>();
		this.size = v;
		this.list_size = 0;
	}
	
	public void generate(){
		for(int i = 0; i < this.size; i++) {
			for(int j = i+1; j < this.size; j++) {
				System.out.println(i + ", " + j);
				this.edge_list.add(new Edge(i, j));
				
			}
		}
		this.list_size = this.edge_list.size();
	}
	
	public void add_to_list(Edge E) {
		this.edge_list.add(E);
		this.list_size++;
	}
	
	public Edge find_E_2(int n1, int n2) {
		for(int i = 0; i < this.list_size; i++) {
			if(this.edge_list.get(i).get_node_1() == n1 && this.edge_list.get(i).get_node_2() == n2) {
				return this.edge_list.get(i);
			}
			else if(this.edge_list.get(i).get_node_1() == n2 && this.edge_list.get(i).get_node_2() == n1) {
				return this.edge_list.get(i);
			}
		}
		return null;
		
	}
	
	public Edge_list find_E_1(int n) {
		Edge_list temp_list = new Edge_list(0);
		for(int i = 0; i < this.list_size; i++) {
			if(this.edge_list.get(i).get_node_1() == n || this.edge_list.get(i).get_node_2() == n) {
				temp_list.add_to_list(this.edge_list.get(i));
			}
		}
		return temp_list;
	}
	
	public int Edge_list_size(){
		return this.list_size;
	}
	
	public boolean edge_probablity_check(int n1, int n2) {
		float temp = this.rand.nextFloat();
		if(this.find_E_2(n1, n2).get_p() < temp) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public void print_list() {
		System.out.print("[");
		for(int i = 0; i < this.list_size; i++) {
			System.out.print(this.edge_list.get(i).get_node_1() + "-" + this.edge_list.get(i).get_node_2());
			System.out.print(", ");
		}
		System.out.print("]\n");
	}
	
	public void print_p() {
		System.out.print("[");
		for(int i = 0; i < this.list_size; i++) {
			System.out.print(this.edge_list.get(i).get_p());
			System.out.print(", ");
		}
		System.out.print("] \n");
	}
}
