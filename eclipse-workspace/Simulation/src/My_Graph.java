import org.jgrapht.graph.*;
import java.io.*;
import java.util.*;
import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("serial")
public class My_Graph<V,E> extends SimpleWeightedGraph<Integer, DefaultWeightedEdge>{

	char[] influence_list;
	boolean[] completion_list;
	int size;
	Random rand = new Random();
	public My_Graph(Class<? extends DefaultWeightedEdge> edgeClass, int n) {
		super(edgeClass);
		this.influence_list = new char[n];
		this.completion_list = new boolean[n];
		this.size = n;
		for(int i = 0; i < this.size; i++) {
			this.influence_list[i] = 'N';
			this.completion_list[i] = false;
		}
		// TODO Auto-generated constructor stub
		
	}
	
	public boolean influencing(int node, char player) {
		if(this.influence_list[node] == 'N' && this.completion_list[node] == false) {
			influence_list[node] = player;
			return true;
		}
		else if(this.influence_list[node] != player && this.completion_list[node] == false){
			if(this.rand.nextFloat()< 0.5) {
				influence_list[node] = player;
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
		
	}
	
	public ArrayList<Integer> check_completion() {
		ArrayList<Integer> output = new ArrayList<Integer>();
		//this.print_currect_status();
		for(int i = 0; i < this.size; i++) {
			if(this.influence_list[i] != 'N' && this.completion_list[i] == false) {
				output.add(i);
				this.completion_list[i] = true;
				//todo: return a int list
			}
		}
		return output;
	}

	public void testing() {
		System.out.println(this.edgesOf(0).toString());
	}
	
	public void probability_check(int node, char player) {
		
		Iterator<DefaultWeightedEdge> iter = this.edgesOf(node).iterator();
		while(iter.hasNext()) {
			DefaultWeightedEdge temp = iter.next();
			int actual_target = this.getEdgeSource(temp);
			if(actual_target == node) {
				actual_target = this.getEdgeTarget(temp);
			}
			if(this.rand.nextFloat() < this.getEdgeWeight(temp) && this.completion_list[actual_target] == false) {
				this.influencing(actual_target, player);
			}
		}
	}
	
	public void print_currect_status(BufferedWriter br, String[] a, String[] d) {
		System.out.println("Current influence list:" + Arrays.toString(this.influence_list));
		System.out.println("Current completion list:" + Arrays.toString(this.completion_list));

		StringBuilder sb = new StringBuilder();
		// Append strings from array
		for (char element : this.influence_list) {
		 sb.append(element);
		 sb.append(",");
		}

		try {
			br.write(Arrays.toString(a) + "," + Arrays.toString(d) + ",");
			br.write(sb.toString());
			br.write("\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void reset() {
		for(int i = 0; i < this.size; i++) {
			this.influence_list[i] = 'N';
			this.completion_list[i] = false;
		}
	}
	
	public void set_complete(int node) {
		this.completion_list[node] = true;
	}
}
