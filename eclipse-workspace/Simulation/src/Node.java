import 	java.util.*;

public class Node {
	int node_num; 
	int v;
	Edge_list connections = new Edge_list(v-1);
	boolean infected = false;
	char A_or_D = 'N';
	public Node(int num, int v){
		this.node_num = num;
		this.v = v;
	}
	
	public Edge_list get_connections(Edge_list full_list) {
		this.connections = full_list.find_E_1(node_num);
		return this.connections;
	}
	
	public float get_P_from_node(int n_num) {
		return this.connections.find_E_2(node_num, n_num).get_p();
	}

	public int get_node_num() {
		return this.node_num;
	}
	
	public boolean get_infected() {
		return this.infected;
	}
	
	public char get_A_or_D() {
		return this.A_or_D;
	}
	
	public void infecting(char infecter) {
		this.infected = true;
		this.A_or_D = infecter;
	}
}
