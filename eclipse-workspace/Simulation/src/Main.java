import org.jgrapht.graph.*;
import java.io.*;
import java.util.*;

public class Main {

	public static void main(String[] args) {
		
		
		Random rand = new Random();
		int n = 10;
		int[] n_set = new int[n];
		My_Graph<Integer, DefaultWeightedEdge> g = new My_Graph<Integer, DefaultWeightedEdge>(DefaultWeightedEdge.class, n);
		for(int i = 0; i < n; i++) {
			g.addVertex(i);
			n_set[i] = i;
			
		}
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				if(i != j) {
					g.addEdge(i, j);
					g.setEdgeWeight(i, j, rand.nextFloat());
				}
			}
		}
		
//		Iterator<Integer> iter = new DepthFirstIterator<>(g);
//        while (iter.hasNext()) {
//            int vertex = iter.next();
//            System.out.println(
//                "Vertex " + vertex + " is connected to: "
//                    + g.edgesOf(vertex).toString());
//            for (int i = 0; i < n-1; i++) {
//            	if(i != vertex) {
//            		System.out.println("Weight of "+ vertex + " and " + i + ": " + g.getEdgeWeight(g.getEdge(vertex, i)) );
//            	}
//            }
//        }
		BufferedWriter buffw = null;
		try {
			buffw = new BufferedWriter(new FileWriter("data/combinations.csv"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int s = 1; s < n/2; s++) {
			Combinations.combinationsChooseR(n_set, s, buffw);
		}
	    
		try {
			buffw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<String[]> combination_list = new ArrayList<String[]>();
		try (BufferedReader buffr = new BufferedReader(new FileReader("data/combinations.csv"))) {
		    String line;
		    while ((line = buffr.readLine()) != null) {
		    	combination_list.add(line.split(","));
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BufferedWriter br = null;
		try {
			br = new BufferedWriter(new FileWriter("data/output.csv"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
        for (int a = 0; a < combination_list.size(); a++) {
        	for (int d = 0; d < combination_list.size(); d++) {
        		if(combination_list.get(a).length == combination_list.get(d).length) {
        			boolean good_2_go = true;
        			for(int t = 0; t < 50; t++) {
        				for(int x = 0; x < combination_list.get(a).length; x++) {
        					g.influencing(Integer.valueOf(combination_list.get(a)[x]), 'A');
        					g.influencing(Integer.valueOf(combination_list.get(d)[x]), 'D');
//        					g.set_complete(Integer.valueOf(combination_list.get(a)[x]));
//        					g.probability_check(Integer.valueOf(combination_list.get(a)[x]), 'A');
        				}
//        				for(int x = 0; x < combination_list.get(d).length; x++) {
//        					if(g.influencing(Integer.valueOf(combination_list.get(d)[x]), 'D')) {
////        						g.set_complete(Integer.valueOf(combination_list.get(d)[x]));
////            					g.probability_check(Integer.valueOf(combination_list.get(d)[x]), 'D');
////            					g.probability_check(Integer.valueOf(combination_list.get(a)[x]), 'A');
////        						good_2_go = true;
//        					}
////        					else {
////        						good_2_go = false;
////        						break;
////        					}
//        					
//        				}
//        				g.check_completion();
//        				if(good_2_go) {
//        					for(int x = 0; x < combination_list.get(d).length; x++) {
//        						g.probability_check(Integer.valueOf(combination_list.get(d)[x]), 'D');
//            					g.probability_check(Integer.valueOf(combination_list.get(a)[x]), 'A');
//        					}
        					ArrayList<Integer> temp = g.check_completion();
        					while(temp.isEmpty() == false) {
        	        	
//        	        			System.out.println("Iteration "+ t);
        						for(int i = 0; i < temp.size(); i++) {
        							g.probability_check(temp.get(i), g.influence_list[temp.get(i)]);
        						}
//        	        		System.out.println("iteration print");
//        	        		g.print_currect_status();
//        	        		t++;
        						temp = g.check_completion();
        					}
        					System.out.println("result of " + Arrays.toString(combination_list.get(a)) + ", " + Arrays.toString(combination_list.get(d)));
        					g.print_currect_status(br, combination_list.get(a), combination_list.get(d));
        					g.reset();
        					
        			}
//        				else {
//        					g.reset();
//        					break;
//        					}
//        				
//        			}
        			if(good_2_go) {
	        			try {
							br.write("\n");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
        			}
        		}
        	}
        }
        
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		
	}
	
	
	
}
