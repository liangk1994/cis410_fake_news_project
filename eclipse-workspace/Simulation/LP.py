#/usr/bin/env python3
# Author: Kenneth Liang(L), Justin Stewart, Franklin Smith
# Reference: https://gist.github.com/WPettersson/287de1e739d4d7869d555fd28ac587cf

# Make sure param.csv and payoff.csv are in the same folder. 
# User "python3 p1.py" to run the program

import cplex
import csv


# Parsing in the data from csv files
import os
import re

file_list = []
for file in os.listdir("data/"):
  if re.match("payoff\d.csv", file):
    file_list.append(file)
    # print(file)

print(file_list)



for f in range(len(file_list)):
  t_payoff = []
  temp_line = []

  with open("data/" + file_list[f], mode="r") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=",")
    t_payoff = list(csv_reader)
  payoff = []
  temp_line = t_payoff[0][0]
  temp_nice_line = []
  for line in t_payoff:
    if(line[0] == temp_line):
      temp_nice_line.append(line)
    else:
      payoff.append(temp_nice_line)
      temp_line = line[0]
      temp_nice_line = []
      temp_nice_line.append(line)
  payoff.append(temp_nice_line)

  targets = len(payoff[0])
  resources = len(payoff[0][0][0])//3+1

  # Promte the user to enter a target and calculate the LP of this target
  # for loop here
  # for attack_num in range(targets):
  # Create an instance of a linear problem to solve
  problem = cplex.Cplex()


  # We want to find a maximum of our objective function
  problem.objective.set_sense(problem.objective.sense.maximize)

  # The names of our variables
  def_variables = []

  for x in range(targets):
    def_variables.append("x" + str(x))
  def_variables.append("v")
  # def_variables.append("C")

  # The obective function. More precisely, the coefficients of the objective
  # function. We build the objective function on the target the attack is going 
  # attack, which is also the Expected Utility of the Defender
  # The last variable is a dummy variable that represent a constant
  # The expresion will be 0 coeffient for the rest of the variable that is not
  # the current target being attack and the (Def_cover - Def_uncover) payoff
  # value of the current target and Def_uncover payoff value for the constant
  # ex. if attack_num = 2 
  # the expression will be (Def_cover - Def_uncover)*(r2) + Def_uncover
  objective = []

  for i in range(targets):
    objective.append(0.0)
  objective.append(1.0)
  # Lower bounds. All of them should be 0 except for the constant. 
  # We want the constant to have a lower bound of 1

  # print("objective: " + str(objective))
  lower_bounds = []
  for i in range(targets):
    lower_bounds.append(0)
  lower_bounds.append(0)
  # lower_bounds.append(1.0)

  # Upper bounds. All of them should be 0 except for the constant. 
  # We want the constant to have a upper bound of 1
  upper_bounds = []
  for i in range(targets):
    upper_bounds.append(1.0)
  upper_bounds.append(cplex.infinity)
  # upper_bounds.append(1.0)


  problem.variables.add(obj = objective,
                        lb = lower_bounds,
                        ub = upper_bounds,
                        names = def_variables)

  # Constraints

  # First, we name the constraints
  constraint_names = []
  for i in range(targets):
    constraint_names.append("c" + str(i))
  constraint_names.append("limit")


  constraints = []

  for a in range(targets):
    temp_coe = [0.0] * (targets)
    for d in range(targets):
      temp_coe[d] = round((float(payoff[a][d][3])),2)
    
    temp_coe.append(-1.0)
    constraints.append([def_variables, temp_coe])
  # This will be the limit constarint where the total value of all the variables
  # cannot be greater than the value of resources
  temp_limit_constarint = []
  for i in range(targets):
    temp_limit_constarint.append(1.0)
  temp_limit_constarint.append(0.0)
  constraints.append([def_variables, temp_limit_constarint])
  # This will be the right hand side of the constraints where they are all 0 
  # other than the last constarint which is the number of resources
  rhs = []

  for i in range(len(constraints)-1):
    rhs.append(0)
  rhs.append(resources)

  # We need to enter the senses of the constraints. That is, we need to tell Cplex
  # whether each constrains should be treated as an upper-limit (≤, denoted "L"
  # for less-than), a lower limit (≥, denoted "G" for greater than) or an equality
  # (=, denoted "E" for equality). In this case, all of the constarint needs to be 
  # greater than 0 except for the last constarint which is less than the number of 
  # resources
  constraint_senses = []
  for i in range(len(constraints)-1): 
    constraint_senses.append("G")

  constraint_senses.append("E")



  # And add the constraints
  problem.linear_constraints.add(lin_expr = constraints,
                                 senses = constraint_senses,
                                 rhs = rhs,
                                 names = constraint_names)

  # Solve the problem
  problem.solve()

  output = []
  for i in range(targets):
    temp_list = []
    temp_list.append(payoff[i][0][0])
    temp_list.append(problem.solution.get_values("x" + str(i)))
    output.append(temp_list)

  print(output)
  # And print the solutions
  # print(problem.solution.get_values())
  with open("data/solution_of "+ file_list[f] , mode='w') as writefile:
    wr = csv.writer(writefile)
    wr.writerows(output)

