# CIS410 Fake News Prevention Project

This project was created to help preventing the spreading of fake news using game theory and a simulation
Author: Kenneth Liang, Justin Stewart

## Installation

Please make sure that Eclipse IDE 2019-03 is installed (https://www.eclipse.org/downloads/packages/installer)
And also python3 is correctly installed with cplex:

```bash
pip3 install cplex
```

## Usage

First import the project in <b>Eclipse IDE</b> and run the Java simulation
Then run the following bash commands for the parsing and calculation:
```bash
cd eclipse-workplace/Simulation
python3 parsing.py
python3 LP.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## More Information
Please refer to the Final Report.pdf for more information of the construction and the progression of the project.
